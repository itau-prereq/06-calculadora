package br.com.itau.calculadora.controllers;

import br.com.itau.calculadora.DTO.RespostaDTO;
import br.com.itau.calculadora.Service.CalculadoraService;
import br.com.itau.calculadora.models.Calculadora;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping("/calculadora")
public class CalculadoraController {

    @Autowired
    private CalculadoraService calculadoraService;

    @GetMapping("/get")
    public String olaMundao(){
        return "ola mundo";
    }

    @PostMapping("/post")
    public String postMundao(){
        return "Post mundo";
    }

    @PostMapping("/somar")
    public RespostaDTO somar(@RequestBody Calculadora calculadora){
        //proxypattern, gera o erro antes de chegar ao service
        if (calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somar com um numero nao rola, parsa");
        } else if(!isNatural(calculadora.getNumeros())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somente numeros naturais sao permitidos");
        }
        return calculadoraService.somar(calculadora);
    }

    @PostMapping("/subtrair")
    public RespostaDTO subtrair(@RequestBody Calculadora calculadora){
        //proxypattern, gera o erro antes de chegar ao service
        if (calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somar com um numero nao rola, parsa");
        }else if(!isNatural(calculadora.getNumeros())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somente numeros naturais sao permitidos");
        }
        return calculadoraService.subtrair(calculadora);
    }

    @PostMapping("/multiplicar")
    public RespostaDTO multiplicar(@RequestBody Calculadora calculadora){
        //proxypattern, gera o erro antes de chegar ao service
        if (calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somar com um numero nao rola, parsa");
        }else if(!isNatural(calculadora.getNumeros())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somente numeros naturais sao permitidos");
        }
        return calculadoraService.multiplicar(calculadora);
    }

    @PostMapping("/dividir")
    public RespostaDTO dividir(@RequestBody Calculadora calculadora){
        //proxypattern, gera o erro antes de chegar ao service
        if (calculadora.getNumeros().size() <= 1){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somar com um numero nao rola, parsa");
        }else if(!isNatural(calculadora.getNumeros())){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Somente numeros naturais sao permitidos");
        }
        return calculadoraService.dividir(calculadora);
    }

    private boolean isNatural(List<Integer> listaNumeros){
        boolean natural = true;
        for(Integer i : listaNumeros){
            if(i < 0){
                natural = false;
            }
        }
        return natural;
    }
}
